<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML 5.0//EN"
"http://www.oasis-open.org/docbook/xml/5.0/dtd/docbook.dtd" [

<!--

Process this file with an XSLT processor: `xsltproc \
-''-nonet /usr/share/xml/docbook/stylesheet/docbook-xsl-ns/\
manpages/docbook.xsl manpage.dbk'.  A manual page
<package>.<section> will be generated.  You may view the
manual page with: man -l <package>.<section>'.  A
typical entry in a Makefile or Makefile.am is:

DB2MAN=/usr/share/sgml/docbook/stylesheet/xsl/nwalsh/\
manpages/docbook.xsl
XP=xsltproc -''-nonet

manpage.1: manpage.dbk
        $(XP) $(DB2MAN) $<
    
The xsltproc binary is found in the xsltproc package.  The
XSL files are in docbook-xsl.  Please remember that if you
create the nroff version in one of the debian/rules file
targets (such as build), you will need to include xsltproc
and docbook-xsl in your Build-Depends control field.

-->

  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "2011-09-16">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "1">
  <!ENTITY dhproduct   "Kwalify">
  <!ENTITY dhpackage   "kwalify">
]>

<refentry xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          version="5.0"
          xml:lang="en"
          xml:id="&dhpackage;">
  <info>
    <author>
	  <firstname>Marc</firstname>
      <surname>Dequènes</surname>
	  <othername role="nickname">`Duck`</othername>
      <email>Duck@DuckCorp.org</email>
	  <contrib>Original author.</contrib>
    </author>
    <copyright>
      <year>2007-2011</year>
      <holder>Marc Dequènes (Duck)</holder>
    </copyright>
	<legalnotice>
    <para>
	  This manual page was written for the <productname>Debian</productname>
	  system (but may be used by others). Permission is granted to copy, distribute
	  and/or modify this document under the terms of the GNU General Public License,
	  Version 3 or any later version published by the Free Software Foundation.
    </para>
	<para>
	  On <productname>Debian</productname> systems, the complete text of the GNU General Public
	  License version 3 can be found in /usr/share/common-licenses/GPL-3.
	</para>
	</legalnotice>
    <date>&dhdate;</date>
  </info>

  <refmeta>
	<refentrytitle>&dhpackage;</refentrytitle>
    <refmiscinfo class="source">&dhproduct;</refmiscinfo>
    <refmiscinfo class="manual">User Commands</refmiscinfo>

    <manvolnum>&dhsection;</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>Tiny schema validator for YAML and JSON</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>

        <group choice="opt">
	  	<arg choice="plain"><option>-h</option></arg>
	  	<arg choice="plain"><option>--help</option></arg>

	  	<arg choice="plain"><option>-v</option></arg>
	</group>

    </cmdsynopsis>
    <cmdsynopsis>
      <command>&dhpackage;</command>

	<arg choice="opt"><option>-s</option></arg>
	<arg choice="opt"><option>-t</option></arg>
	<arg choice="opt"><option>-l</option></arg>
	<arg choice="opt"><option>-E</option></arg>

	<arg choice="plain"><option>-f</option> schema.yaml</arg>
	<arg choice="plain">doc.yaml</arg>
	<arg choice="opt">doc2.yaml ...</arg>

    </cmdsynopsis>
    <cmdsynopsis>
      <command>&dhpackage;</command>

	<arg choice="opt"><option>-s</option></arg>
	<arg choice="opt"><option>-t</option></arg>
	<arg choice="opt"><option>-l</option></arg>
	<arg choice="opt"><option>-E</option></arg>

	<arg choice="plain"><option>-m</option> schema.yaml</arg>
	<arg choice="opt">schema2.yaml ...</arg>

    </cmdsynopsis>
    <cmdsynopsis>
      <command>&dhpackage;</command>

	<arg choice="opt"><option>-s</option></arg>
	<arg choice="opt"><option>-t</option></arg>
	<arg choice="opt"><option>-l</option></arg>
	<arg choice="opt"><option>-E</option></arg>

	<group choice="req">
		<arg choice="plain"><option>-a</option> action</arg>
		<arg choice="plain"><option>-ha</option> action</arg>
	</group>
	<arg choice="plain"><option>-f</option> schema.yaml</arg>
	<arg choice="opt">schema2.yaml ...</arg>

    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1>
    <title>DESCRIPTION</title>

    <para><command>&dhpackage;</command> validates schema file, or YAML / JSON file against a schema.</para>

  </refsect1>

  <refsect1>
    <title>OPTIONS</title>

    <variablelist>
      <varlistentry>
        <term><option>-h</option>
          <option>--help</option>
        </term>
        <listitem>
          <para>Show summary of options.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option>
        </term>
        <listitem>
          <para>Display version information.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-s</option></term>
	<listitem>
	  <para>Be silent.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-t</option></term>
	<listitem>
	  <para>Expand tab characters.</para>
	</listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-l</option></term>
	<listitem>
	  <para>Show linenumber when errored (experimental).</para>
	</listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-E</option></term>
	<listitem>
	  <para>Show errors in emacs-style (implies '-l').</para>
	</listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-m</option> <parameter>schema.yaml</parameter></term>
        <listitem>
          <para>Meta-validation mode: validate schema.yaml.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-f</option> <parameter>schema.yaml</parameter></term>
        <listitem>
          <para>Schema definition file: validate file against schema.yaml.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-a</option> <parameter>action</parameter></term>
        <listitem>
          <para>Generate code (depending on action: 'genclass-ruby' or 'genclass-java').</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-ha</option> <parameter>action</parameter></term>
        <listitem>
          <para>Generate code (depending on action: 'genclass-ruby' or 'genclass-java') with properties.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><parameter>doc.yaml</parameter></term>
        <listitem>
          <para>File to validate.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
</refentry>

